/**
 * Project Name:webcrawler-sourceer
 * File Name:HuabanProcessor.java
 * Package Name:com.zongtui.webcrawler.sourceer.crawler.webmagic
 * Date:2015-4-29下午6:33:08
 * Copyright (c) 2015, 众推项目组版权所有.
 *
 */

package com.zongtui.webcrawler.sourceer.crawler.webmagic;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;

import com.zongtui.webcrawler.webmagic.downloader.SeleniumDownloader;

/**
 * ClassName: HuabanProcessor <br/>
 * Function: HuabanProcessor. <br/>
 * date: 2015-4-29 下午6:33:08 <br/>
 *
 * @author feng
 * @version 
 * @since JDK 1.7
 */
public class HuabanProcessor implements PageProcessor {

    private Site site;

    @Override
    public void process(Page page) {
        page.addTargetRequests(page.getHtml().links().regex("http://huaban\\.com/.*").all());
        if (page.getUrl().toString().contains("pins")) {
            page.putField("img", page.getHtml().xpath("//div[@id='pin_img']/img/@src").toString());
        } else {
            page.getResultItems().setSkip(true);
        }
    }

    @Override
    public Site getSite() {
        if (site == null) {
            site = Site.me().setDomain("huaban.com").addStartUrl("http://huaban.com/").setSleepTime(1000);
        }
        return site;
    }

    public static void main(String[] args) {
        Spider.create(new HuabanProcessor()).thread(5)
//                .scheduler(new RedisScheduler("localhost"))
                .pipeline(new ConsolePipeline())
                .downloader(new SeleniumDownloader("/Users/yihua/Downloads/chromedriver"))
                .run();
    }
}

