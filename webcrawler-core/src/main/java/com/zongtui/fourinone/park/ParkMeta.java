package com.zongtui.fourinone.park;

/**
 * park的属性。【park的意思为一个节点集】
 */
public class ParkMeta {
    /**
     * 元信息.
     */
    private static String meta = "meta.";
    /**
     * 版本号.
     */
    private static String version = "meta.version";

    /**
     * 创建者.
     */
    private static String createBy = "meta.createBy";

    /**
     * 创建ip.
     */
    private static String createIp = "meta.createIp";

    /**
     * 创建时间.
     */
    private static String createTime = "meta.createTime";

    /**
     * 授权.
     */
    private static String auth = "meta.auth";


    /**
     * 属性.
     */
    private static String prop = "meta.prop";

    /**
     * 更新者.
     */
    private static String updateBy = "meta.updateBy";

    /**
     * 更新ip.
     */
    private static String updateIp = "meta.updateIp";


    /**
     * 更新时间.
     */
    private static String updateTime = "meta.updateTime";

    /**
     * 心跳.
     */
    private static String heartbeat = "meta.heartbeat";


    public static String getMeta() {
        return meta;
    }

    public static String getVersion() {
        return version;
    }

    public static String getCreateBy() {
        return createBy;
    }

    public static String getCreateIp() {
        return createIp;
    }

    public static String getCreateTime() {
        return createTime;
    }

    public static String getAuth() {
        return auth;
    }

    public static String getProp() {
        return prop;
    }

    public static String getUpdateBy() {
        return updateBy;
    }

    public static String getUpdateIp() {
        return updateIp;
    }

    public static String getUpdateTime() {
        return updateTime;
    }

    public static String getHeartbeat() {
        return heartbeat;
    }


    public static String getMeta(String dns) {
        return dns + meta;
    }

    public static String getVersion(String dns) {
        return dns + version;
    }

    public static String getCreateBy(String dns) {
        return dns + createBy;
    }

    public static String getCreateIp(String dns) {
        return dns + createIp;
    }

    public static String getCreateTime(String dns) {
        return dns + createTime;
    }

    public static String getAuth(String dns) {
        return dns + auth;
    }

    public static String getProp(String dns) {
        return dns + prop;
    }

    public static String getUpdateBy(String dns) {
        return dns + updateBy;
    }

    public static String getUpdateIp(String dns) {
        return dns + updateIp;
    }

    public static String getUpdateTime(String dns) {
        return dns + updateTime;
    }


    public static void main(String[] args) {
        System.out.println("ParkMeta.getVersion():" + getVersion());
    }
}